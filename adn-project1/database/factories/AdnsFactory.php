<?php

namespace Database\Factories;

use App\Models\Adns;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdnsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Adns::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
