<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adns extends Model
{
    protected $fillable = ["adn", "hasmitation"];
}
